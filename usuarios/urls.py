from django.urls import path
from . import views

app_name = 'usuarios'
urlpatterns = [
    path('index/', views.index, name='index'),

    path('index/index_registro_empresa/',
         views.index_registro_empresa, name='index_registro_empresa'),

    path('index/post_registro_empresa', views.post_registro_empresa,
         name='post_registro_empresa'),

    path('index/index_datos_registro', views.index_datos_registro,
         name='index_datos_registro'),

    path('index/post_registro_empresa/datos_registro/', views.index_datos_registro,
         name='datos_registro'),

    # coordinador

    path('coordinador/',
         views.coordinador, name='coordinador'),

    path('coordinador/registrar_estudiante/', views.registrar_estudiante,
         name='registrar_estudiante'),

    path('coordinador/post_registro_estudiante/', views.post_registro_estudiante,
         name='post_registro_estudiante'),

    path('coordinador/lista_estudiantes/', views.lista_estudiantes,
         name='lista_estudiantes'),

    path('coordinador/lista_empresas/', views.lista_empresas,
         name='lista_empresas'),

    path('coordinador/historico_convocatorias/', views.historico_convocatorias,
         name='historico_convocatorias'),

    path('coordinador/lista_convocatorias_empresa/', views.lista_convocatorias_empresa,
         name='lista_convocatorias_empresa'),

    path('coordinador/actualizar_semestre/', views.actualizar_semestre,
         name='actualizar_semestre'),




    # empresa

    path('empresa/',
         views.empresa, name='empresa'),

    path('empresa/registrar_convocatoria/', views.registrar_convocatoria,
         name='registrar_convocatoria'),

    path('empresa/post_registro_convocatoria/', views.post_registro_convocatoria,
         name='post_registro_convocatoria'),

    path('empresa/lista_convocatorias/', views.lista_convocatorias,
         name='lista_convocatorias'),

    path('empresa/lista_convocatorias/editar_convocatoria/', views.editar_convocatoria,
         name='editar_convocatoria'),



    # Estudiante

    path('estudiante/',
         views.estudiante, name='estudiante'),

    path('estudiante/convocatorias_abiertas/', views.convocatorias_abiertas,
         name='convocatorias_abiertas'),

    path('estudiante/actualizar_perfil/', views.actualizar_perfil,
         name='actualizar_perfil'),

    path('estudiante/post_actualizar_perfil/', views.post_actualizar_perfil,
         name='post_actualizar_perfil'),





]
