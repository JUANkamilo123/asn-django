from django.db import models
from django.db.models.fields import DateTimeField
import datetime

# Create your models here.


class registro(models.Model): ##registro empresa
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    razon_social = models.CharField(max_length=60)
    ciudad = models.CharField(max_length=50)
    nombre_contacto = models.CharField(max_length=50)
    email = models.EmailField()


class crear_estudiante(models.Model):
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    nombre_completo = models.CharField(max_length=50)
    apellido_completo = models.CharField(max_length=50)
    email = models.EmailField()

class crear_convocatoria(models.Model):
    nombre = models.CharField(max_length=30)
    fecha_inicial = models.CharField(max_length=30)
    fecha_final = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=500)

class editar_perfil(models.Model):
    email = models.EmailField
    fortalezas = models.CharField(max_length=100)
    herramientas = models.CharField(max_length=100)


