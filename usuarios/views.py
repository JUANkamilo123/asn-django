from django.shortcuts import redirect, render
from usuarios.models import registro, crear_estudiante, crear_convocatoria, editar_perfil
from django.http import request
from django.http.response import HttpResponse
from django.http import HttpResponse

# Create your views here.


def index(request):

    return render(request, "index/index.html")


def index_registro_empresa(request):

    return render(request, "index/index_registro_empresa.html")


def index_datos_registro(request):

    return render(request, "index/index_datos_registro.html")


def post_registro_empresa(request):
    username = request.POST['username']
    password = request.POST['password']
    razon_social = request.POST['razon_social']
    ciudad = request.POST['ciudad']
    nombre_contacto = request.POST['nombre_contacto']
    email = request.POST['email']

    # Crear el objeto

    user = registro(username=username, password=password, razon_social=razon_social,
                    ciudad=ciudad, nombre_contacto=nombre_contacto, email=email)
    user.save()

    return HttpResponse("Inscripción Enviada al Servidor")


# coordinador

def coordinador(request):
    return render(request, "coordinador/coordinador.html")


def registrar_estudiante(request):
    return render(request, "coordinador/crear_estudiante/crear_estudiante.html")


def post_registro_estudiante(request):
    username = request.POST['username']
    password = request.POST['password']
    nombre_completo = request.POST['nombre_completo']
    apellido_completo = request.POST['apellido_completo']
    email = request.POST['email']

    # Crear el objeto

    registro_estudiante = crear_estudiante(username=username, password=password, nombre_completo=nombre_completo,
                                           apellido_completo=apellido_completo, email=email)
    registro_estudiante.save()

    return HttpResponse("Inscripción Enviada al Servidor")


def lista_estudiantes(request):

    # Obtiene a los estudiantes registrados
    lista = crear_estudiante.objects.all()
    # crear el contexto
    contexto = {
        'lista_registro_estudiantes': lista
    }
    return render(request, "coordinador/lista_estudiantes/lista_estudiantes.html", contexto)


def lista_empresas(request):

    # Obtiene a los estudiantes registrados
    lista = registro.objects.all()
    # crear el contexto
    contexto = {
        'lista_registro_empresas': lista
    }

    return render(request, "coordinador/lista_empresas/datos_empresa.html", contexto)


def historico_convocatorias(request):

    # Obtiene a los estudiantes registrados
    lista = crear_convocatoria.objects.all()
    # crear el contexto
    contexto = {
        'lista_historico_convocatorias': lista
    }

    return render(request, "coordinador/histo_conv/hist_conv.html", contexto)


def lista_convocatorias_empresa(request):

    # Obtiene a los estudiantes registrados
    lista = crear_convocatoria.objects.all()
    # crear el contexto
    contexto = {
        'lista_historico_convocatorias': lista
    }

    return render(request, "coordinador/lista_empresas/lista_convocatorias.html", contexto)


def actualizar_semestre(request):
    return render(request, "coordinador/actualizar_semestre/semestre.html")

    # Empresa


def empresa(request):
    return render(request, "empresa/empresa.html")


def convocatorias_abiertas(request):
    return render(request, "estudiante/convocatorias_abiertas/convocatorias_abiertas.html")


def post_registro_convocatoria(request):
    nombre = request.POST['nombre']
    fecha_inicial = request.POST['fecha_inicial']
    fecha_final = request.POST['fecha_final']
    descripcion = request.POST['descripcion']

    # Crear el objeto

    registro_convocatoria = crear_convocatoria(nombre=nombre, fecha_inicial=fecha_inicial, fecha_final=fecha_final,
                                               descripcion=descripcion)
    registro_convocatoria.save()

    return HttpResponse("Registro convocatoria Enviada al Servidor")


def lista_convocatorias(request):

    # Obtiene a los estudiantes registrados
    lista = crear_convocatoria.objects.all()
    # crear el contexto
    contexto = {
        'lista_convocatorias': lista
    }
    return render(request, "empresa/lista_conovocatorias/lista_convocatorias.html", contexto)


def editar_convocatoria(request):
    return render(request, "empresa/lista_conovocatorias/editar_convocatoria.html")

    # Estudiante


def estudiante(request):
    return render(request, "estudiante/estudiante.html")


def registrar_convocatoria(request):
    return render(request, "empresa/convocatoria/crear_convocatoria.html")


def post_registro_convocatoria(request):
    nombre = request.POST['nombre']
    fecha_inicial = request.POST['fecha_inicial']
    fecha_final = request.POST['fecha_final']
    descripcion = request.POST['descripcion']

    # Crear el objeto

    registro_convocatoria = crear_convocatoria(nombre=nombre, fecha_inicial=fecha_inicial, fecha_final=fecha_final,
                                               descripcion=descripcion)
    registro_convocatoria.save()

    return HttpResponse("Registro convocatoria Enviada al Servidor")


def lista_convocatorias(request):

    # Obtiene a los estudiantes registrados
    lista = crear_convocatoria.objects.all()
    # crear el contexto
    contexto = {
        'lista_convocatorias': lista
    }
    return render(request, "empresa/lista_conovocatorias/lista_convocatorias.html", contexto)


def actualizar_perfil(request):
    return render(request, "estudiante/editar_perfil.html")


def post_actualizar_perfil(request):
    email = request.POST['email']
    fortalezas = request.POST['fortalezas']
    herramientas = request.POST['herramientas']

    # Crear el objeto

    perfil = editar_perfil(
        email=email, fortalezas=fortalezas, herramientas=herramientas)
    perfil.save()
    return HttpResponse("Registro convocatoria Enviada al Servidor")


def actualizado(request):

    actualizado = editar_perfil.objects.last()
    contexto = {
        'perfil_actualizado': actualizado
    }

    return render(request, "estudiante/estudiante.html", contexto)
